TOP_LOCAL_PATH := $(call my-dir)
include $(call all-subdir-makefiles)
#Restore LOCAL_PATH, $(CLEAR_VARS) not clear the variable LOCAL_PATH
LOCAL_PATH := $(TOP_LOCAL_PATH)
include $(CLEAR_VARS)
#Activate to use tests

OPENCV_CAMERA_MODULES := on
OPENCV_INSTALL_MODULES := on
OPENCV_LIB_TYPE:=SHARED
OPENCV_PATH:=/home/carlos/Desktop/tools/OpenCV-2.4.9-android-sdk/sdk/native/jni/OpenCV.mk
$(info $(LOCAL_SRC_FILES))
include $(OPENCV_PATH)

BINARY=$(BUILD_SHARED_LIBRARY)
LOCAL_MODULE := tracker
#Common packages
LOCAL_SRC_FILES += ../../../../utils/core.cpp
#Filters 
LOCAL_SRC_FILES += ../../../../gradient_mser/gradient_mser_filter.cpp

LOCAL_SRC_FILES += ../../../../contour3/sanitizer.cpp
LOCAL_SRC_FILES += ../../../../contour3/fast_contour_filter3.cpp

LOCAL_SRC_FILES += ../../../../focus/focus_filter.cpp
LOCAL_SRC_FILES += ../../../../classifier/classif_filter.cpp
#Tracker class
LOCAL_SRC_FILES +=../tracker_wrap.cpp 
$(info $(LOCAL_SRC_FILES))

LOCAL_CFLAGS := -std=c++11
LOCAL_C_INCLUDES += $(LOCAL_PATH)  ../ ../../../
LOCAL_C_INCLUDES += ../../../utils
LOCAL_C_INCLUDES += ../../../gradient_mser
LOCAL_C_INCLUDES += ../../../contour3
LOCAL_C_INCLUDES += ../../../focus
LOCAL_C_INCLUDES += ../../../classifier

LOCAL_LDLIBS     += -llog -ldl
LOCAL_LDLIBS    += -landroid


include $(BINARY)
