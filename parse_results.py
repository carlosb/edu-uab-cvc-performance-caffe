#!/usr/bin/python
import sys

NAME_FILE="data.bump"
class Test(object):
    def __init__(name_test):
        self.name_test = name_test
        self.results = []

class Result(object):
    def __init__(detections,time):
        self.detections = detections
        self.time = time

def is_name_test(line):
    return (line.startswith('TEST::RESULT'))
def is_time(line):
    return (line.startswith('TEST::TIME'))
def is_end(line):
    return (line.startswith('RESULT:###'))
def get_name_test(line):
    return line.split("RESULT:")[1]
def get_time(line):
    return line.split("TIME:")[1].split("=")[1].split(" ")[0]
def get_result(line):
    name=line.split(":")[1]
    perc=line.split(":")[2]
    return (name,perc)

import pickle

def serialize_results (results,name_file):
    with open(name_file,"wb") as output:
        pickle.dump(results,output)

def deserialize_results(name_file):
    with open(name_file,"rb") as input:
        return pickle.load(input)


def main():
#for arg in sys.argv:
#    print arg
    arguments = sys.argv;
    if len(arguments) <2:
        return 1

    #file with logs
    path_file = arguments[1]
    #dump info
    if len(arguments)>2:

        name_file=arguments[2] 
    else:
        name_file=NAME_FILE

    #get lines
    with open(path_file) as f:
        content =  f.readlines()


    test_info = [line.rstrip('\n') for line in content if (line.startswith('TEST') or 'RESULT'  in line) ]
    #test_info = [line for line in content]

    all_results = {}
    current_name_test = None
    list_results = []
    print "-----------------------------"
    for line in test_info:
        #print line
        if is_name_test(line): 
            current_name_test = get_name_test(line).rstrip()
            if current_name_test.startswith("/sdcard/"):
                current_name_test = current_name_test.replace("/sdcard/","")
            if current_name_test.startswith("../"):
                current_name_test = current_name_test.replace("../","")
            print "name_test: "+current_name_test
            
            #fix problems. check if it is necessary add or not info
            list_results = []
        elif is_time(line):
            ne_time = get_time(line)
            #list_results.append(next_time)
        elif is_end(line):
            if not all_results.has_key(current_name_test):
    	        all_results[current_name_test]=list_results
            else:
    	        results = all_results[current_name_test]
                results.extend(list_results)
    	        all_results[current_name_test] = results            
        else:
            result = get_result(line)
            list_results.append(result)
    
    print "-----------------------------"
    serialize_results(all_results,name_file)
    

#    for key,value in all_results.iteritems():
#        print "key="+str(key)
#        print "values:"
#        for pair_value in value: 
#            print "name="+str(pair_value[0])+" perc="+str(pair_value[1])



if __name__=="__main__":
    main();
