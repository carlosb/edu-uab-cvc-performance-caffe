#!/usr/bin/python
import sys
import pickle


arguments = sys.argv

if len(arguments)<2:
    print "comparative_data.py data.dump"
    sys.exit()


name_file=arguments[1]

class Test(object):
    def __init__(name_test):
        self.name_test = name_test
        self.results = []

class Result(object):
    def __init__(detections,time):
        self.detections = detections
        self.time = time
def serialize_results (results,name_file):
    with open(name_file,"wb") as output:
        pickle.dump(results,output)
def deserialize_results(name_file):
    with open(name_file,"rb") as input:
        return pickle.load(input)

all_results = deserialize_results(name_file)

for key,value in all_results.iteritems():
	print "key="+str(key)
        print "values:"
        print str(value)


