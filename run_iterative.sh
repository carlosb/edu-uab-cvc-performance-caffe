#!/bin/sh

MYPWD=`pwd`
BUILD_DIR=${MYPWD}/build
#bvlc_reference_caffenet.caffemodel  deploy-cynny-20.prototxt  deploy.prototxt  labels-cynny-20.txt  labels.txt  mean.binaryproto  mean-cynny-20.binaryproto  model-cynny-20.caffemodel"
cp -r ./model/model-cynny-20.caffemodel build/bvlc_reference_caffenet.caffemodel 
cp -r ./model/deploy-cynny-20.prototxt build/deploy.prototxt
cp -r ./model/labels-cynny-20.txt build/labels.txt 
cp -r ./model/mean-cynny-20.binaryproto  build/mean.binaryproto

cd "${BUILD_DIR}"
END=10
for i in $(seq 1 $END);
do
	echo "trying $i"
	./caffe_example ../cynny_samples 1
	echo "----------------------------------------"
done;
cd ..
