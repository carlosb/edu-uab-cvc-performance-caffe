#include "classifier.h"

#include <dirent.h>
#include <deque>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "chronotimer.h"

#include <iostream>
#include <sstream>


void load_model(Classifier & classifier) {
#ifdef ANDROID
		std::string model_file("/sdcard/model/deploy.prototxt");
		std::string trained_file("/sdcard/model/bvlc_reference_caffenet.caffemodel");
		std::string mean_file("/sdcard/model/mean.binaryproto");
		std::string label_file("/sdcard/model/labels.txt");
#else
		std::string model_file("deploy.prototxt");
		std::string trained_file("bvlc_reference_caffenet.caffemodel");
		std::string mean_file("mean.binaryproto");
		std::string label_file("labels.txt");

#endif 
		classifier.loadModel(model_file,trained_file,mean_file,label_file);
}

void unload_model(Classifier & classifier) {
		classifier.unloadModel();
}

void print_results(std::vector<Prediction> & results) {
	for (int index=0; index<results.size(); index++) {
		Prediction prediction = results[index];
		std::cout <<"RESULT:"<<prediction.first << ":" << prediction.second << std::endl;	
	}
	std::cout<<"RESULT:###"<<std::endl;
}


bool is_correct_path(struct dirent *epdf) {
	std::string name(epdf->d_name);
	return (name != "." && name != "..");
}

bool is_directory(struct dirent *epdf) {
	return  (epdf->d_type == DT_DIR);
}

std::deque<std::string> list_all_files (std::string & root_path) {
		std::deque<std::string> directories_to_analyse;
		std::deque<std::string> files_to_analyse;

		directories_to_analyse.push_back(root_path);

		while (directories_to_analyse.size() !=0) {
			std::cout << "Number of files to analyse: "<<directories_to_analyse.size() << std::endl;
			const char *path = directories_to_analyse.back().c_str();
			std::string new_root_path = std::string(path);
			directories_to_analyse.pop_back();
			DIR *dpdf;
			struct dirent *epdf;
			dpdf = opendir(path);
			if (dpdf != NULL) {
				while (epdf=readdir(dpdf)) {
					if (is_correct_path(epdf)) {
						if (is_directory(epdf)) {
							std::string complete_path = new_root_path+std::string("/")+std::string(epdf->d_name);
							directories_to_analyse.push_front(complete_path);
						}
						else {
							std::string complete_path = new_root_path+std::string("/")+std::string(epdf->d_name);
							files_to_analyse.push_front(complete_path);
						}
					}
				}
			}
			closedir(dpdf);
		}
		return files_to_analyse;

}


void test_and_print_images(std::deque<std::string> files_to_analyse, Classifier & classifier) {
		int num_results = 5;
		cvc::utils::Timer timer;
		for (int i = 0; i < files_to_analyse.size(); i++) {
			std::string  path = files_to_analyse[i];
			
			std::cout<<"TEST::RESULT:"<<path<<std::endl;
			
			cv::Mat image = cv::imread(path);
			timer.start();
			std::vector<Prediction> results = classifier.classify(image,num_results);
			double time_seconds = timer.stop();
			std::cout << "TEST::TIME:msecs="<<time_seconds<<" msecs "<<std::endl;
			print_results(results);
		}
}

int pointer_char_to_int(const char * char_nums) {
	stringstream strValue;
	strValue << char_nums;
	unsigned int intValue;
	strValue >> intValue;
	return intValue;
}

int NUM_TESTS = 30;

int main (int argc, char **argv) {

		if (argc < 3) {
			std::cout << " caffe_example directory_images num_tests" << std::endl;
			return -1;
		}

		const char* path = argv[1];
		const char* char_num_tests = argv[2];

		int num_tests = pointer_char_to_int(char_num_tests);

		std::string root_path(path);
		std::deque<std::string> files_to_analyse = list_all_files(root_path);

		std::cout << "Arrive here!!" << std::endl;
		cvc::utils::Timer timer;
		timer.start();
		std::cout << "Arrive here!!2" << std::endl;
		Classifier classifier;
		std::cout << "Arrive here!!3" << std::endl;
		load_model(classifier);
		std::cout << "Arrive here!!4" << std::endl;
		std::cout << "TEST::TIME:initialization="<<timer.stop()<<" msecs "<<std::endl;
		
		int size_files = files_to_analyse.size();

		for (int i = 0; i< num_tests; i++) {
			test_and_print_images(files_to_analyse,classifier);
		}
		
		timer.start();
		unload_model(classifier);	
		std::cout << "TEST::TIME:unload="<<timer.stop()<<" msecs "<<std::endl;
		
		/*
		while (files_to_analyse.size() !=0) {
			std::string filename = files_to_analyse.back();
			files_to_analyse.pop_back();
			std::cout << "RESULT::->"<< filename <<std::endl;
		} 
		*/
	
		return 0;	
	
		
	//	std::string name("bambini1");
	//	cv::Mat image = cv::imread(name+std::string(".jpg"));
	//	std::vector<Prediction> results = classifier.classify(image,5);

		//print_results(name,results);



}
