#!/usr/bin/env sh
set -e



if [ -z "$NDK_ROOT" ] && [ "$#" -eq 0 ]; then
     echo "Applying desktop version without NDK_ROOT"
     DESKTOP="1"
else
    NDK_ROOT="${1:-${NDK_ROOT}}"
    DESKTOP="0"
fi

BLAS=eigen
if [ "$#" -eq 2  ] && [ "$2" = "--open" ] ; then
	echo "Applying openblas"
	BLAS=open
fi 	

ANDROID_ABI=${ANDROID_ABI:-"armeabi-v7a with NEON"}
MYPWD=`pwd`
N_JOBS=${N_JOBS:-4}



#USE_OPENBLAS=${USE_OPENBLAS:-0}
#if [ ${USE_OPENBLAS} -eq 1 ]; then
#    if [ "${ANDROID_ABI}" = "armeabi-v7a-hard-softfp with NEON" ]; then
#        OpenBLAS_HOME=${ANDROID_LIB_ROOT}/openblas-hard
#    elif [ "${ANDROID_ABI}" = "armeabi-v7a with NEON"  ]; then
#        OpenBLAS_HOME=${ANDROID_LIB_ROOT}/openblas-android
#    else
#        echo "Error: not support OpenBLAS for ABI: ${ANDROID_ABI}"
#        exit 1
#    fi

#    BLAS=open
#    export OpenBLAS_HOME="${OpenBLAS_HOME}"
#else
#    BLAS=eigen
#    export EIGEN_HOME="${ANDROID_LIB_ROOT}/eigen3"
#fi


BUILD_DIR=${MYPWD}/build


rm -rf "${BUILD_DIR}"
mkdir -p "${BUILD_DIR}"
cd "${BUILD_DIR}"
if [ "$DESKTOP" = "0"  ]; then

cmake -DCMAKE_TOOLCHAIN_FILE="${MYPWD}/android.toolchain.cmake" \
      -DANDROID_NDK="${NDK_ROOT}" \
      -DCMAKE_BUILD_TYPE=Release \
      -DANDROID_ABI="${ANDROID_ABI}" \
      -DANDROID_NATIVE_API_LEVEL=21 \
      -DANDROID_USE_OPENMP=ON \
      -DBUILD_python=OFF \
      -DBUILD_docs=OFF \
      -DCPU_ONLY=ON \
      -DUSE_GLOG=OFF \
      -DUSE_LMDB=OFF \
      -DUSE_LEVELDB=OFF \
      -DUSE_HDF5=OFF \
      -DBLAS=${BLAS} \
      ..
else 
 cmake ..
fi

make -j${N_JOBS}

cd "${PWD}"
