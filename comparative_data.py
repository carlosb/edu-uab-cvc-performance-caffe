#!/usr/bin/python
import pickle
import sys

arguments = sys.argv

if len(arguments)<4:
    print "comparative_data.py input_file1.dump input_file2.dump input_file3.dump"
    sys.exit()


input_file1=arguments[1]
input_file2=arguments[2]
input_file3=arguments[3]


class Test(object):
    def __init__(name_test):
        self.name_test = name_test
        self.results = []

class Result(object):
    def __init__(detections,time):
        self.detections = detections
        self.time = time
def serialize_results (results, name_file):
    with open(name_file,"wb") as output:
        pickle.dump(results,output)
def deserialize_results(name_file):
    with open(name_file,"rb") as input:
        return pickle.load(input)

input_file1_results = deserialize_results(input_file1)
input_file2_results = deserialize_results(input_file2)
input_file3_results = deserialize_results(input_file3)

for key1,value1 in input_file1_results.iteritems():
        print "key: "+str(key1)
        

        key1="/sdcard/"+key1+'\r'
        value2 = input_file2_results[key1]
        value3=  input_file3_results[key1]
       
        print "file 1: "
        for pair_value in value1: 
            print "name="+str(pair_value[0])+" perc="+str(pair_value[1])
       
        if value2:
            print "file 2: "
            for pair_value in value2: 
                print "name="+str(pair_value[0])+" perc="+str(pair_value[1])

        if value3:
            print "file 3: "
            for pair_value in value3: 
                print "name="+str(pair_value[0])+" perc="+str(pair_value[1])
       
        print "KEY="+str(key1)+'\n'
        print "DIFERENCE: EIGEN-OPENBLAS  name="+str(value2[0][0])+" values="+str(float(value2[0][1])-float(value3[0][1]))
        print "DIFERENCE: DESKTOP-OPENBLAS name="+str(value1[0][0])+" values="+str(float(value1[0][1])-float(value3[0][1]))
        #print "key="+str(key)
        #print "values:"
        #print str(value)


