#!/bin/sh

UPDATE="NO"
OPENBLAS="NO"
while getopts ":uo" opt; do
	case $opt in
		u)
			UPDATE="YES"
			;;
		o)
			OPENBLAS="YES"
			;;
		\?) 
			echo "Invalid option -$OPTARG" 
			;;
	esac
done


	if  [ "$UPDATE" = "YES" ];
	then
		echo "Updating model information...."
		find ./bvlc_reference_caffenet.caffemodel -exec adb push {} /data/local/tmp \;
		find ./mean.binaryproto -exec adb push {} /data/local/tmp \;
		find ./deploy.prototxt -exec adb push {} /data/local/tmp \;
		find ./labels.txt -exec adb push {} /data/local/tmp \;
		
		find ./third_parties/opencv/libopencv_java.so -exec adb push {} /data/local/tmp \;
		
		echo "Updated information"
		shift
	fi
	if [ "$OPENBLAS" = "YES" ]; then
		echo "Applying openblas"
		find ./libs_openblas/libcaffe.so -exec adb push {} /data/local/tmp \;
	else
		echo "Applying eigen"
		find ./libs_eigen/libcaffe.so -exec adb push {} /data/local/tmp \;
	fi


#find ../libs/armeabi-v7a/* -exec adb push {} /data/local/tmp \;
find build/caffe_example -exec adb push {} /data/local/tmp \;
#push files
END=10
for i in $(seq 1 $END);
do
	echo "trying $i"
	adb shell LD_LIBRARY_PATH="/data/local/tmp:/vendor/lib:/system/lib" /data/local/tmp/caffe_example /sdcard/cynny_samples 1
	echo "----------------------------------------"
done;
