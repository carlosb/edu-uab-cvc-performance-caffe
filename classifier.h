#ifndef CLASSIFIER
#define CLASSIFIER

#include <string>
#include <vector>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <iomanip>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "caffe/caffe.hpp"

using namespace caffe;
//using namespace std::string;

typedef std::pair<std::string,float> Prediction;

class Classifier {
	private:
		shared_ptr<Net<float> > net_;
		cv::Size input_geometry_;
		int num_channels_;
		cv::Mat mean_;
		std::vector<std::string> labels_;
		int N = 4;
		std::string model_file_;

		void SetMean(const std::string& mean_file);
		std::vector<float> Predict(const cv::Mat& img);
		void WrapInputLayer(std::vector<cv::Mat>* input_channels);
		void Preprocess(const cv::Mat& img,std::vector<cv::Mat>* input_channels);
	public:
		void loadModel(std::string model_file_
			, std::string trained_file_
			, std::string mean_file_
			, std::string label_file_);
		void unloadModel();	
		std::vector<Prediction> classify(cv::Mat & img, int num_results);
};

#endif
